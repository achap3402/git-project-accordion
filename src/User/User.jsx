import React from "react";
import"../App.scss";

const User = ({name, username, email, phone, website, button, content, toggle, item}) => {
    return (
        <div className={item}>
            <div className="title" onClick={toggle}>
                <h2>{name}</h2>
                <span className={button}></span>
            </div>
            <div className={content}>
                <p>username: {username}</p>
                <p>email: {email}</p>
                <p>phone: {phone}</p>
                <p>website: {website}</p>
            </div>
        </div>
    )
}

export default User;