import React, {useEffect, useState} from 'react';
import './App.scss';
import User from './User/User';


const App = () => {
  const [activeTab, setActiveTab] = useState(null);
  const [items, setItems] = useState([]);
  const toggle = (index) => {
    activeTab === index ? setActiveTab(null) : setActiveTab(index)
  }

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(json => setItems(json))
  }, [])
  return (
    <div className="app">
      <div className='main-title-wrapper'>
        <div className='main-title'>
          <h1>Accordion</h1>
          <div className="deco-line"></div>
          <p>react<span>&</span>fetch</p>
        </div>
      </div>
      <div className='accordion'>
        {items.map((item, index) => 
          <User 
            key={item.id} 
            name={item.name}
            username={item.username}
            email={item.email}
            phone={item.phone}
            website={item.website}
            button={activeTab === index ? 'btn btn--close' : 'btn'}
            content={activeTab === index ? 'content content--show' : 'content'}
            item={activeTab === index ? 'item item--show' : 'item'}
            toggle={() => toggle(index)}
          />
        )}
      </div>
    </div>
  );
}

export default App;